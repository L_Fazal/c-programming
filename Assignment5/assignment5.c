#include <stdio.h>
int avgNoOfPeople = 120;
float avgTicketPrice = 15.00;
int costOfAShow = 500;
int costPerPerson = 3;
int noOfPeople(float ticketPrice);
float findProfit (float ticketPrice);
int main(){
	float ticketPrice;
	puts("Enter ticket price");
	scanf("%f", &ticketPrice);
	printf("Number of people coming: %d\n", noOfPeople(ticketPrice));
	printf("Ticket price: %.2f\n", ticketPrice);
	printf("Total profit: %.2f\n", findProfit(ticketPrice));
}
int noOfPeople(float ticketPrice){
	float ticketPriceDifferance;
		//average ticketPrice is avgTicketPrice
	ticketPriceDifferance = ticketPrice - avgTicketPrice;
	if (ticketPriceDifferance < 0){
		return avgNoOfPeople +  (int)ticketPriceDifferance * 4 * -1;
	}
	else{
		return avgNoOfPeople - (int)ticketPriceDifferance * 4;
	}
}
float findProfit (float ticketPrice){
	return (noOfPeople(ticketPrice)*ticketPrice) - (costOfAShow + costPerPerson*noOfPeople(ticketPrice));
}




