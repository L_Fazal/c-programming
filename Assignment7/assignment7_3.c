#include <stdio.h>
void addMatrices();
void multiplyMatrices();
int rowA, colA, rowB, colB;
/*****Adding two matrices*****/
int main(){
    int i, j, matrixA[5][5], matrixB[5][5];
    

    puts("***Matrix A***");
    puts("Enter number of rows and columns: (max = 5)\n(***row<space>column***)");
    scanf("%d %d", &rowA, &colA);
    for(i = 0; i < rowA; i++){
        printf("Row %d\n", i +1);
        for(j = 0; j < colA; j++){
            printf("Element %d\n", j +1);
            scanf("%d", &matrixA[i][j]);
        }
    }

    puts("***Matrix B***");
    puts("Enter number of rows: (max = 5)\n(***row<space>column***)");
    scanf("%d %d", &rowB, &colB);
    for(i = 0; i < rowB; i++){
        printf("Row %d\n", i +1);
        for(j = 0; j < colB; j++){
            printf("Element %d\n", j +1);
            scanf("%d", &matrixB[i][j]);
        }
    }

    addMatrices(matrixA, matrixB);
    multiplyMatrices(matrixA, matrixB);

    return 0;
}


void addMatrices(int arrayA[5][5], int arrayB[5][5]){
    int i,j;
    if((rowA == rowB) && (colA == colB)){
        printf("\n");
        puts("Addition of both matrices are:");
        for(i =0; i < rowA; i++){
            for(j =0; j < colA; j++){
                printf("%d\t", arrayA[i][j] + arrayB[i][j]);
            }
            printf("\n");
        }
        printf("\n");
    }
    else
        puts("Cannot add matrices.(Wrong Dimensions)");
}

void multiplyMatrices(int arrayA[5][5], int arrayB[5][5]){
    int i,j, k, result = 0;
    if(colA == rowB){
        printf("\n");
        puts("Multiplication of both matrices are:");
        for (i = 0; i < rowA; i++) {
            for (j = 0; j < colB; j++) {
                for (k = 0; k < colA; ++k) {
                    result += arrayA[i][k] * arrayB[k][j];
                }
                printf("%d\t", result);
                result = 0;
            }
            printf("\n");
        }
    }
    else
        puts("Cannot multiply matrices.(Wrong dimensions)");
}
