#include <stdio.h>

int main(){
	char sentence[100], check;
	int i, count = 0;
	puts("Enter a sentence");
	fgets(sentence, sizeof(sentence), stdin);
	puts("Enter a letter to check frequency");
	scanf("%c" , &check);
	for(i = 0;;i++){
		if(sentence[i] == '\0')
			break;
		else{
			if(sentence[i] == check)
				count ++;
		}
	}
	printf("\"%c\" is repeated %d times.\n", check, count);
	return 0;
}
