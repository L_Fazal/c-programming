#include <stdio.h>
#include <string.h>

int main(){
	int i;
	char sentence[100];
	puts("Enter a sentence to reverse it:");
	fgets(sentence, sizeof(sentence), stdin);
	for(i = strlen(sentence) - 2; i >= 0; i--){
		printf("%c", sentence[i]);
	}
	puts("");
	return 0;
}
