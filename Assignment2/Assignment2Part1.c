#include <stdio.h>
//reading and multiplying two floating point numbers
int main() {
	float num1, num2, result;
	printf("Enter a number :\n");
	scanf("%f", &num1);
	printf("Enter another number :\n");
	scanf("%f", &num2);
	result = num1 * num2;
	printf("%f multiplied by %f is : %f\n", num1, num2, result);
	return 0;
}	
