#include <stdio.h>
//Calculating the area of a disc with a given radius.
int main() {
	float radius, PI, area;
	PI = 3.14159;
	printf("Enter radius of disk :(cm)\n");
	scanf("%f", &radius);
	area = PI * radius * radius;
	printf("The area of the disk is %.4f cm2.\n", area);	
	return 0;
}

