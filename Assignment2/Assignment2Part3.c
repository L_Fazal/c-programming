#include <stdio.h>
//swapping two integers
int main() {
	int num1, num2, buffer;
	printf("Enter first number :\n");
	scanf("%d", &num1);
	printf("Enter second number :\n");
	scanf("%d", &num2);
	printf("Your numbers will be swapped\n");
	buffer = num1;
	num1 = num2;
	num2 = buffer;
	printf("First number is %d\nSecond number is %d\n", num1, num2);
	return 0;
}	

