#include <stdio.h>

struct stuProfile{
    char stuName[50];
    char subName[20];
    int marks;
};
void input(struct stuProfile s);
int main() {
    int ent = 1, count = 0;
    struct stuProfile s[50];

    for(int i=0; ent; i++){
        printf("Enter name: ");
        scanf("%s", s[i].stuName);
        printf("Enter subject name: ");
        scanf("%s", s[i].subName);
        printf("Enter marks: ");
        scanf("%d", &(s[i].marks));
        if(i >= 4 && count < 50){
            puts("Do you want to enter more?");
            printf("Enter 1(for YES) or 0(for NO): ");
            scanf("%d", &ent);
        }
        count++;
    }
    puts("");
    
    
    for(int i=0; i<count; i++){
        puts(s[i].stuName);
        puts(s[i].subName);
        printf("%d\n\n", s[i].marks);
    }
    return 0;
}
