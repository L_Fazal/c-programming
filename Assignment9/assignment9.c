#include <stdio.h>

int main()
{
    char str[100];
    FILE *ptr;
// Writing into file
    ptr = fopen("assignment9.txt", "w");
    fputs("UCSC is one of the leading institutes in Sri Lanka for computing studies. ", ptr);
    fclose(ptr);
// Reading from file
    ptr = fopen("assignment9.txt", "r");
    while (fgets(str, sizeof(str), ptr) != NULL)
    {
        printf("%s", str);
    }
    fclose(ptr);
// Appending text into file
    ptr = fopen("assignment9.txt", "a");
    fputs("\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields. ", ptr);
    fclose(ptr);

    return 0;
}