#include <stdio.h>

int main() {
	int num;
	printf("Enter a number :");
	scanf("%d", &num);
	if(num == 0)
		printf("Your number %d is zero\n", num);
	if(num > 0)
		printf("Your number %d is positive\n", num);
	if(num < 0)
		printf("Your number %d is negative\n", num);

	return 0;
}	
