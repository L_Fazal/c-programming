#include <stdio.h>

void printLine(int num){
	if(num > 0){
		printf("%d ", num);
		printLine(num -1);
	}
	else
		printf("\n");
}
void pattern(int input){
	if(input>1){
		pattern(input -1);
	}
	printLine(input);
}

int main() {
	int userInput;
	puts("Enter an integer value:");
	scanf("%d", &userInput);
	pattern(userInput);
	return 0;
}




