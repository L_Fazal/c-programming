#include <stdio.h>

int fibonacci(int n){
	if(n == 0){
		return 0; 
	}
	else if(n == 1){
		return 1;
	}
	else{
		return fibonacci(n-1) + fibonacci(n-2);
	}
}
void printFibonacci(int m){
	if(m > 0){
		printFibonacci(m -1);
	}
	printf("%d\n", fibonacci(m));
}

int main(){
	int number;
	puts("Enter a positive integer:");
	scanf("%d", &number);
	printFibonacci(number);
	return 0;
}
